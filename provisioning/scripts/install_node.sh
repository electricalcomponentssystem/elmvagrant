#!/bin/bash


if which nodejs > /usr/bin/nodejs; then
	echo "NodeJS on jo asennettuna..."

else
	echo "Asennetaan nodejs..."
	sudo apt-get install -y curl
	curl -sL https://deb.nodesource.com/setup | sudo bash -
	sudo apt-get install -y nodejs
	apt-get install -y build-essential
		 
fi