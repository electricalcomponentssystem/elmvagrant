#!/bin/bash

if which mongo > /usr/bin/mongo; then
	echo "MongoDB already exists."

else
	echo "Importing the public key.."
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

	echo "Creating a list file for MongoDB.."
	echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list

	echo "Updating.."
	sudo apt-get update

	echo "Installing the latest stable version of MongoDB"
	sudo apt-get install --force-yes mongodb-org
	sudo apt-get install --force-yes mongodb-org=2.6.1 mongodb-org-server=2.6.1 mongodb-org-shell=2.6.1 mongodb-org-mongos=2.6.1 mongodb-org-tools=2.6.1

	echo "mongodb-org hold" | sudo dpkg --set-selections
	echo "mongodb-org-server hold" | sudo dpkg --set-selections
	echo "mongodb-org-shell hold" | sudo dpkg --set-selections
	echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
	echo "mongodb-org-tools hold" | sudo dpkg --set-selections

	echo "Setting the $PATH variable"
	sudo mkdir -p /data/db/
	sudo chown -R vagrant:vagrant /data/db/

	echo ".................."
	touch startup.sh
	echo "sudo service mongod stop" >> startup.sh && echo "sudo mongod" >> startup.sh
	sudo chmod +x startup.sh
	echo "Run startup.sh to start mongob server."
fi